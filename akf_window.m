function [ x_win ] = akf_window(x)
%AKF_WINDOW Summary of this function goes here
%   Detailed explanation goes here
    X = fft(x', 2^nextpow2(2*length(x)-1));                                                % Mindestens doppelt so gro�e Datenbereich im FFT Raum anlegen. Warum?
    Cr = abs(X).^2;                                                                        % Betrag der komplexen, transformierten Amplitute quadrieren.
    c = real(ifft(Cr));                                                                    % Realteil der inversen FFT ermitteln.
    acor_ifftfft = shiftdim(c,1);                                                          % Dimension anpassen.
    acor_ifftfft = acor_ifftfft(1:length(x));                                              % Zuschnitt um nur den positiven Bereich der AKF zu erhalten
    
    acor_ifftfft = acor_ifftfft(1:floor(length(x)*0.75));                                  % Zuschnitt um nur "guten" bereich der AKF zu erhalten
    periodendauer =  myperiodendauer(acor_ifftfft);
    usefull_samples = periodendauer * floor(length(x)/periodendauer);
    x_win = x(1:usefull_samples);                                                        % AKF Fenster
end

