classdef messung
    %MESSUNG Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Ort
        Format
        Type
        Points
        Count
        XIncrement
        XOrigin
        XReference
        YIncrement
        YOrigin
        YReference
        VoltsPerDiv
        Offset
        SecPerDiv
        Delay
        X
        Y
        RawData
        preambleBlock
    end
    
    methods
        function obj = messung(Ort, preambleBlock, RawData)
            % Maximum value storable in a INT16
            maxVal = 2^16; 
            obj.RawData = RawData;
            obj.Ort = Ort;
            
            %  split the preambleBlock into individual pieces of info
            obj.preambleBlock = regexp(preambleBlock,',','split');

            % store all this information into a waveform structure for later use
            obj.Format = str2double(preambleBlock{1});     % This should be 1, since we're specifying INT16 output
            obj.Type = str2double(preambleBlock{2});
            obj.Points = str2double(preambleBlock{3});
            obj.Count = str2double(preambleBlock{4});      % This is always 1
            obj.XIncrement = str2double(preambleBlock{5}); % in seconds
            obj.XOrigin = str2double(preambleBlock{6});    % in seconds
            obj.XReference = str2double(preambleBlock{7});
            obj.YIncrement = str2double(preambleBlock{8}); % V
            obj.YOrigin = str2double(preambleBlock{9});
            obj.YReference = str2double(preambleBlock{10});
            obj.VoltsPerDiv = (maxVal * obj.YIncrement / 8);      % V
            obj.Offset = ((maxVal/2 - obj.YReference) * obj.YIncrement + obj.YOrigin);         % V
            obj.SecPerDiv = obj.Points * obj.XIncrement/10 ; % seconds
            obj.Delay = ((obj.Points/2 - obj.XReference) * obj.XIncrement + obj.XOrigin); % seconds

            % Generate X & Y Data
            obj.X = (obj.XIncrement.*(1:length(obj.RawData))) - obj.XIncrement;
            obj.Y = (obj.YIncrement.*(obj.RawData - obj.YReference)) + obj.YOrigin; 
        end
    end
end

