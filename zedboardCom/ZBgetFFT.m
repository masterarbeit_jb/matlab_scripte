function [ real, imag ] = ZBgetFFT(ZB, CH)
%ZBGETFFT Summary of this function goes here
%   Detailed explanation goes here
    cmd = '\0';
    if (strcmp(CH, 'A'))
        cmd = 'l';
    end
    if (strcmp(CH, 'B'))
        cmd = 'm';
    end
    
    try
        fprintf(ZB,cmd);
    catch MException
    end;

    %wait for bytes being available
    while(ZB.BytesAvailable == 0)
    end;
    real = fread(ZB, 2048, 'float');
    imag = fread(ZB, 2048, 'float');

end

