function ZBdisconnect( ZB )
%ZBCONNECT Trennt die Verbindung mit dem ZedBoard.
%   ZBdisconnect( ZB ) Trennt das in ZB angegebene ZedBoard
%   See also ZBCONNECT
fclose(ZB)
delete(ZB)
clear ZB
end

