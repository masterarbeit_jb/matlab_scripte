function [ amp, f ] = ZBgetVeffsb( ZB, CH)
%ZBSETVEFFSB Summary of this function goes here
%   Detailed explanation goes here

    cmd_eff = '\0';
    cmd_f = '\0';
    if (strcmp(CH, 'A'))
        cmd_eff = 'j';
        cmd_f = 'w';
    end
    if (strcmp(CH, 'B'))
        cmd_eff = 'k';
        cmd_f = 'x';
    end
    
    
    try
        fprintf(ZB,cmd_eff);
    catch MException
    end;

    %wait for bytes being available
    while(ZB.BytesAvailable == 0)
    end;

    amp = fread(ZB, 1, 'float');
    
        try
        fprintf(ZB,cmd_f);
    catch MException
    end;

    %wait for bytes being available
    while(ZB.BytesAvailable == 0)
    end;

    f = fread(ZB, 1, 'float');
end


