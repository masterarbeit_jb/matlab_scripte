function [ data ] = ZBgetCHraw(ZB, CH)
%ZBGETCHA Summary of this function goes here
%   Detailed explanation goes here
    cmd = '\0';
    if (strcmp(CH, 'A'))
        cmd = 'a';
    end
    if (strcmp(CH, 'B'))
        cmd = 'b';
    end
    
    try
        fprintf(ZB,cmd);
    catch MException
    end;

    %wait for bytes being available
    while(ZB.BytesAvailable == 0)
    end;

    data = fread(ZB, 2048, 'float');
end

