function [ ZB ] = ZBconnect( COMPORT )
%ZBCONNECT Verbindet sich mit dem ZedBoard.
%   ZB = ZBconnect(COMPORT) Verbindet sich mit dem in COMPORT angegebenen
%   COM-Port.
%   See also ZBDISCONNECT, ZBNEWMESSUREMENT

ZB = serial(COMPORT,'BaudRate',115200);
ZB.InputBufferSize = 2048*4*2;
ZB.OutputBufferSize = 2048*4*2;
if (strcmp(ZB.Status,'open')  ~= 1)
    try
        fopen(ZB);
    catch MException
        fclose(ZB);
        fopen(ZB);
    end;
end;

end

