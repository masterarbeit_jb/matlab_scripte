function [ t_ZB ] = ZBsetSampletime( ZB, sampletime )
%ZBSETSAMPLETIME Summary of this function goes here
%   Detailed explanation goes here
Fs = 100e6;
t_fs_range = 2048/Fs;
vorteiler = (sampletime)/(t_fs_range);
vorteiler = min([max([ceil(vorteiler) 4]) 2^13-1]);
t_fs_range = t_fs_range*vorteiler;
ZBsetOS(ZB, vorteiler);
t_ZB = (0:2047)*t_fs_range/2048;
end

