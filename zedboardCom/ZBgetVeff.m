function [ amp ] = ZBgetVeff( ZB, CH)
%ZBGETAMP Summary of this function goes here
%   Detailed explanation goes here
    cmd = '\0';
    if (strcmp(CH, 'A'))
        cmd = 'h';
    end
    if (strcmp(CH, 'B'))
        cmd = 'i';
    end
    
    try
        fprintf(ZB,cmd);
    catch MException
    end;

    %wait for bytes being available
    while(ZB.BytesAvailable == 0)
    end;

    amp = fread(ZB, 1, 'float');
end

