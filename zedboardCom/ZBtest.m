%% Oszi einrichten
oszi = keysight_oszi('USB0::0x0957::0x17B2::MY53390382::0::INSTR');
%Labels vergeben
set_chan_labels(oszi, 'S1', 'NC', 'A+(S)', 'A-(S)');
%%
A = 15e-3;  % Amplitude
f = 1e3;    % Frequenz

% Frequenzgeneratoren konfigurieren,
dualgen(oszi, f, A)
%% 
ZB = ZBconnect('COM4');
%%
%Multiplexer auf S1 und S2 einstellen.
ZBsetMux(ZB,1,2,1,2);

%Downsampling berechnen
Fs_adc = 100e6;                 % Abtastrate ADC
periods = 20;                    % Wunsch Perioden im Datensatz
time = periods/f;               % Notwendige Messzeit
ds_fac =  time/(2048/Fs_adc);   % DS-Faktor
DS = round(log2(ds_fac));       % log2 vom DS-Faltor
ZBsetDS(ZB,DS);                 % Einstellen der Downsamplingrate.
t = ((0:2047)/Fs_adc)*2^DS;

% Verst�rkung einstellen
% Verf�gbare Verst�rkungen
V_array = [1.5370629371 2.4014598540 3.6122448980 5.6545454545 8.5146771037 13.1518987342 20.5918367347 31.2362204724 49.7928843710 76.1467710372 116.6626506024 179.6046511628 275.2857142857 423.4422442244 651.8474576271 1003.6109660574];
V_stufe = 9;               % Ausgew�hlte Stufe
V_sel = V_array(V_stufe);   % V, der ausgew�hlten stufe
ZBsetAmp(ZB, V_stufe);      % Verst�rkung einstellen

%% Messung durchf�hren
ZBnewmessurement(ZB);
% Rohdaten empfangen & plotten
CHA = ZBgetCHraw(ZB, 'A');  % Rohdaten CH A (Sensor) auslesen
%%CHA = CHA/V_sel;            % Rohdaten CH A (Sensor) mit V skalieren.
CHB = ZBgetCHraw(ZB, 'B');  % Rohdaten CH B (Emitter) auslesen
plot(CHA);
hold on;
plot(CHB);
hold off;
%%
plot(t, CHA)                % Plot
grid on;
title('Rohdaten der sensierenden Elektroden')
ylabel('U [V]')
xlabel('t [s]')
ylim([-1 1]*A*1.4);
xlim([t(1) t(end)])
% FFT empfangen & plotten 
[ real, imag ]  = ZBgetFFT(ZB,'A');
CHA_fft = sqrt(real.^2+imag.^2);
CHA_fft = (CHA_fft/V_sel)*2;
CHA_fftcut = CHA_fft(1:1024)
f_fft = (0:1023)*(Fs_adc/2048)/(2^DS);
loglog(f_fft, CHA_fftcut);
title('Fouriertransformation der Rohdaten (gefenstert)');
xlabel('f[Hz]')
ylabel('Amplitude [V]')
grid on;
% selektiven Effektivwert anzeigen
ZB_effektivwert = ZBgetVeff(ZB,'A')/V_sel;
ZB_Amplitude = ZB_effektivwert*sqrt(2)
%%
ZBdisconnect(ZB);