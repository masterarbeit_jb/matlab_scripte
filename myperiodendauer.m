function [ periodendauer, periodenzahl ] = myperiodendauer( y )
%MYPERIODENDAUER Summary of this function goes here
%   Detailed explanation goes here

last_zero = 0;
first_zero = 0;
zero_counter = 0;

for i = 1:length(y)-1
    if(sign(y(i)) ~= sign(y(i+1)))
        if first_zero == 0
            first_zero = i;
        end
       last_zero = i; 
       zero_counter = zero_counter + 1;
    end
end
periodenzahl = (zero_counter-1)/2;
periodendauer = round(((last_zero - first_zero-1)/periodenzahl));


end
