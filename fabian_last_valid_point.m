function [ last_valid_datapoint ] = fabian_last_valid_point(signal)
%FABIAN_LAST_VALID_POINT Summary of this function goes here
%   Detailed explanation goes here

    %// computing number of periods to be stored in memory
    number_of_periods = floor(length(signal)/fabian_zeros_computation(signal));
    number_of_samples = fabian_zeros_computation(signal);

    last_valid_datapoint = number_of_periods*number_of_samples;

end

