% Verbindung mit dem Roboterhilfsprogramm herstellen
rob = init();
connect(rob);
%Verbindung mit dem Zedboard herstellen
ZB = ZBconnect('COM4');
%% Roboter 
m2home(rob)
m2messfeld(rob)
t_messung = 2;
%%
f = 1000;
ZBsetMux(ZB,1,3,1,3);
%Downsampling berechnen
Fs_adc = 100e6;                 % Abtastrate ADC
periods = 20;                   % Wunsch Perioden im Datensatz
time = periods/f;               % Notwendige Messzeit
ds_fac =  time/(2048/Fs_adc);   % DS-Faktor
DS = round(log2(ds_fac));       % log2 vom DS-Faltor
ZBsetDS(ZB,DS);                 % Einstellen der Downsamplingrate.
t = ((0:2047)/Fs_adc)*2^DS;
% Verst�rkung einstellen
% Verf�gbare Verst�rkungen
V_array = [1.5370629371 2.4014598540 3.6122448980 5.6545454545 8.5146771037 13.1518987342 20.5918367347 31.2362204724 49.7928843710 76.1467710372 116.6626506024 179.6046511628 275.2857142857 423.4422442244 651.8474576271 1003.6109660574];
V_stufe = 1;                   % Ausgew�hlte Stufe
V_sel = V_array(V_stufe);      % V, der ausgew�hlten stufe
ZBsetAmp(ZB, V_stufe);         % Verst�rkung einstellen
%%

min = -50;
max = 50;
dif = 10;
steps = ((max-min)/dif+1)
messungen = steps^2;
dauer = (messungen * t_messung)/60

clear eff xpos ypos rawA rawB
eff = zeros(steps, steps);
for y = 1:steps
    for x = 1:steps
        aktuelle_xpos = min + (x-1)*dif;
        aktuelle_ypos = min + (y-1)*dif;
        xpos(x,y) = aktuelle_xpos;
        ypos(x,y) = aktuelle_ypos;
    end
end
%%
tic()
mittelwert = zeros(steps, steps);
figure(1)
for x = 1:steps
    for y = 1:steps
        aktuelle_xpos = min + (x-1)*dif;
        aktuelle_ypos = min + (y-1)*dif;
        m2position(rob,aktuelle_xpos,aktuelle_ypos,0);
        % Messung durchf�hren
        ZBnewmessurement(ZB);
        % Rohdaten empfangen & plotten
        CHA = ZBgetCHraw(ZB, 'A');  % Rohdaten CH A (Sensor) auslesen
        CHA = CHA/V_sel;            % Rohdaten CH A (Sensor) mit V skalieren.
        %CHB = ZBgetCHraw(ZB, 'B');  % Rohdaten CH B (Emitter) auslesen
        ZB_effektivwert = ZBgetVeff(ZB,'A')/V_sel;
        messwert(x,y) = ZB_effektivwert;
        mittelwert(x,y) = mean(CHA);
        rawA(x,y,:) = CHA;
        %rawB(x,y,:) = CHB;
        [f, Y] = smartfft(CHA, 1/(t(2)-t(1)));
        eff(x,y) = Y(22);
        xpos(x,y) = aktuelle_xpos;
        ypos(x,y) = aktuelle_ypos;
        contourf(xpos, ypos, mittelwert,10)
        title('Mittelwert')
        drawnow
    end
end
t_messung = toc/messungen;
m2position(rob,0,0,0);
%%
subplot(2,2,1)
contourf(xpos, ypos, mittelwertS1S2,10)
subplot(2,2,2)
contourf(xpos, ypos, mittelwertS1,10)
subplot(2,2,3)
contourf(xpos, ypos, mittelwertS2,10)
subplot(2,2,4)
contourf(xpos, ypos, mittelwertS2S1,10)
%%
for x = 1:steps
    for y = 1:steps
        messmean(x,y) = mean(rawA(x,y,:));
        myeff(x,y) = rms(rawA(x,y,:)-messmean(x,y));
        myeffmO(x,y) = rms(rawA(x,y,:));

    end
end
contourf(xpos, ypos, fftfiltered );
colorbar
%%
subplot(2,2,1)
contourf(xpos, ypos, eff,10)
title('FFT ermittelter Effektivwert')
subplot(2,2,2)
contourf(xpos, ypos, myeffmO,10)
title('Effektivwert mit Offset')
subplot(2,2,3)
contourf(xpos, ypos, myeff,10)
title('Effektivwert ohne Offset')
subplot(2,2,4)
contourf(xpos, ypos, messmean,10)
title('Offset')

%%
ZBnewmessurement(ZB);
CHA = ZBgetCHraw(ZB, 'A');  % Rohdaten CH A (Sensor) auslesen
subplot(2,1,1)
plot(t,CHA/V_sel)
[f_fft Y] = smartfft(CHA, 1/(t(2)-t(1)));
subplot(2,1,2)
plot(f_fft, Y)
mean(CHA)*1e3
%%
selx=11;
sely= 11;
 plot(squeeze(rawA(selx,sely,:)))
 title(sprintf('Pos: (%d| %d); U_{Eff}: %0.0e V', xpos(selx, sely), ypos(selx, sely), eff(selx, sely)))
%%


plot(t, CHA)                % Plot
hold on
plot(t, CHB)                % Plot
hold off
grid on;
title('Rohdaten der sensierenden Elektroden')
ylabel('U [V]')
xlabel('t [s]')

xlim([t(1) t(end)])
%% FFT empfangen & plotten 
[ real, imag ]  = ZBgetFFT(ZB,'A');
CHA_fft = sqrt(real.^2+imag.^2);
CHA_fft = (CHA_fft/V_sel)*2;
CHA_fftcut = CHA_fft(1:1024);
f_fft = (0:1023)*(Fs_adc/2048)/(2^DS);
semilogx(f_fft, CHA_fftcut);
title('Fouriertransformation der Rohdaten (gefenstert)');
xlabel('f[Hz]')
ylabel('Amplitude [V]')
grid on;
% selektiven Effektivwert anzeigen
ZB_effektivwert = ZBgetVeff(ZB,'A')/V_sel
ZB_Amplitude = ZB_effektivwert*sqrt(2)