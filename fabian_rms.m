function [ rms ] = fabian_rms( signal)
%FABIAN_RMS Summary of this function goes here
  % Detailed explanation goes here
    temp = 0;
    for t = 1:(fabian_last_valid_point(signal))
        temp = temp + signal(t)^2;
    end
    temp = temp/fabian_last_valid_point(signal);
    rms = sqrt(temp);
end

