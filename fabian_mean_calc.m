function [ mean ] = fabian_mean_calc( signal )
%FABIAN_MEAN_CALC Summary of this function goes here
%   Detailed explanation goes here
    temp = 0;
    for t = 1:(fabian_last_valid_point(signal))
        temp = temp + signal(t);
    end
    mean = temp/fabian_last_valid_point(signal);
end

