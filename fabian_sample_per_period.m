function [ number_of_samples ] = fabian_sample_per_period( signal )
t = 0;
overflow_error = 0;
for i = 1:length(signal)-1
    %// if a zero position has been detected -> save it in array
    if( or(and(signal(i) >= 0,signal(i+1) < 0),and(signal(i) <= 0,signal(i+1) > 0)))
        
        zeroPositions(t+1)=i;
        if (t<99)
            t = t+1;
        else
            overflow_error = overflow_error+1;
        end
    end
end
for i= 1:t-1
    %compute the distances of each zero position to it's neighbour and multiply it by two to get samples per period
    zeroDistances(i) = 2*(zeroPositions(i+1)-zeroPositions(i));
end
% average all samples per period to get a higher robustness
number_of_samples = round(mean(zeroDistances));