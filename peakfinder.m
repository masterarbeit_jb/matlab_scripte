function [ peak_index ] = peakfinder( signal )
%PEAKFINDER Summary of this function goes here
%   Detailed explanation goes here
peak = signal(1);
peak_index = 1;
for i = 1:(length(signal)-2)
    if (peak<signal(i))
        peak = signal(i);
        peak_index = i;
    end
end
end

