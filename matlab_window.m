function [ x_window_bb, x_window_sb] = matlab_window(x, matlab_win)
%MATLAB_WINDOW Summary of this function goes here
%   Detailed explanation goes here
        windowfunction = str2func(cell2mat(matlab_win));
        if ~(strcmp(cell2mat(matlab_win),'rectwin'))
            window =  windowfunction(length(x), 'periodic');          % Fenster erzeugen
        else
            window =  windowfunction(length(x));          % Fenster erzeugen
        end
        x_window = window'.*x;                                                   % Fenster verwenden
        x_window_bb = x_window /rms(window);                                        % Fensterdämpfung ausgleichen
        x_window_sb = x_window * length(x)/sum(window);                            % Korrekturfaktor wegen Fensterdämpfung
end

