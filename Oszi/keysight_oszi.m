classdef keysight_oszi
    %Keysight (Agilent) Osziloskope ansteuern
    %   Detailed explanation goes here
    
    properties
        rsrcname
        kohandle
        wgen
        labels
    end
    
    methods
        %Construktor
        function obj = keysight_oszi(rsrcname)
            %Construktor
            obj.rsrcname = rsrcname;
            obj.kohandle = visa('agilent',obj.rsrcname);
            % Set the buffer size
            obj.kohandle.InputBufferSize = 500000;
            % Set the timeout value
            obj.kohandle.Timeout = 10;
            % Set the Byte order
            obj.kohandle.ByteOrder = 'littleEndian';
            % Open the connection
            fopen(obj.kohandle);
            %Standard Einstellungen...
            set_timebase(obj, 'MAIN');
            set_acquire_type(obj, 'HRESolution', 1);
            %set_acquire_type(obj, 'NORMal', 1);
        end
        function delete(obj)
            fclose(obj.kohandle);
            delete(obj.kohandle);
        end
        function send_cmd(obj, cmd)
            fprintf(obj.kohandle,cmd);
        end
        function return_value = get_query(obj, myquery)
            return_value = query(obj.kohandle,myquery);
        end
        function wavegen(obj, freq, wavefunction, Ueff, Uoff)
            obj.wgen.freq = freq;
            obj.wgen.wavefunction = wavefunction;
            obj.wgen.Ueff = Ueff;
            obj.wgen.Uoff = Uoff;
            
            send_cmd(obj, sprintf(':WGEN:FREQuency %f', obj.wgen.freq)); % Frequenz einstellen
            send_cmd(obj, sprintf(':WGEN:FUNCtion %s', obj.wgen.wavefunction)); % Signalverlauf vorgeben
            send_cmd(obj, sprintf(':WGEN:VOLTage %f', (obj.wgen.Ueff * 2 * sqrt(2)))); % Amplitude vorgeben (in Vpp), daher Veff umrechnen.
            send_cmd(obj, sprintf(':WGEN:VOLTAGE:OFFSET %f', obj.wgen.Uoff)); % Offset vorgeben
        end
        
        
        function dualgen(obj, freq,Amp)
            send_cmd(obj, sprintf(':WGEN1:FREQuency %f', freq)); % Frequenz einstellen
            send_cmd(obj, sprintf(':WGEN1:FUNCtion %s', 'SINUSOID')); % Signalverlauf vorgeben
            A_min = 10e-3;
            if(Amp <A_min)
                wgen2_phi = 2*asin(Amp/(-2*A_min))*180/pi;
                send_cmd(obj, sprintf(':WGEN1:TRACk:FREQuency  %d', 0));
                send_cmd(obj, sprintf(':WGEN1:VOLTage %f', 2*(A_min))); % Amplitude vorgeben (in Vpp)
                send_cmd(obj, sprintf(':WGEN2:VOLTage %f', 2*(A_min))); % Amplitude vorgeben (in Vpp)
                send_cmd(obj, sprintf(':WGEN2:FUNCtion %s', 'SINUSOID')); % Signalverlauf vorgeben
                send_cmd(obj, sprintf(':WGEN1:TRACk:FREQuency  %d', 1));
                send_cmd(obj, sprintf(':WGEN1:TRACk:PHASe   %f', 0));
                send_cmd(obj, sprintf(':WGEN2:TRACk:PHASe   %f', wgen2_phi));
            else
                send_cmd(obj, sprintf(':WGEN1:VOLTage %f', 2*(Amp))); % Amplitude vorgeben (in Vpp)
                send_cmd(obj, sprintf(':WGEN1:TRACk:FREQuency  %d', 0)); % Tracking deaktivieren.
                
                send_cmd(obj, sprintf(':WGEN2:FUNCtion %s', 'DC')); % Signalverlauf vorgeben
                send_cmd(obj, sprintf(':WGEN2:OFFSET %f', 0)); 
            end
            
        end       
        function set_wavegen_state(obj, state)
            send_cmd(obj, sprintf(':WGEN:OUTPut %d', state)); % Ein = 1; Aus = 0
        end
        function set_timebase(obj, mode)
            send_cmd(obj, sprintf(':TIMEBASE:MODE %s', mode));
        end
        function set_acquire_type(obj, type, count)
            send_cmd(obj, sprintf(':ACQUIRE:TYPE %s', type));
            send_cmd(obj, sprintf(':ACQUIRE:COUNT %d', count));
        end
        function set_chan_labels(obj, CH1, CH2, CH3, CH4)
            send_cmd(obj, sprintf(':CHAN1:LAB "%s"', CH1));
            send_cmd(obj, sprintf(':CHAN2:LAB "%s"', CH2));
            send_cmd(obj, sprintf(':CHAN3:LAB "%s"', CH3));
            send_cmd(obj, sprintf(':CHAN4:LAB "%s"', CH4));
        end
        function set_all_channels_on(obj)
            send_cmd(obj, ':WAVEFORM:SOURCE CHAN1'); 
            send_cmd(obj, ':WAVEFORM:SOURCE CHAN2'); 
            send_cmd(obj, ':WAVEFORM:SOURCE CHAN3'); 
            send_cmd(obj, ':WAVEFORM:SOURCE CHAN4');
        end
        function set_chan_range_to_bestfit(obj, time_range)
            send_cmd(obj, sprintf(':TIM:RANG %f',time_range));
            send_cmd(obj, ':RUN');
            for chan = 1:4
                send_cmd(obj, sprintf(':CHAN%d:RANG %f',chan, 8*1));
                send_cmd(obj, sprintf(':CHAN%d:OFFS %f',chan, 0));
                vmin = str2double(get_query(obj, sprintf(':MEASure:VMIN? CHAN%d', chan)));
                vmax = str2double(get_query(obj, sprintf(':MEASure:VMAX? CHAN%d', chan)));
                send_cmd(obj, sprintf(':CHAN%d:RANG %f',chan, (vmax-vmin)*4));
                send_cmd(obj, sprintf(':CHAN%d:OFFS %f',chan, 30e-3+((vmin+vmax)/2)));
                vmin = str2double(get_query(obj, sprintf(':MEASure:VMIN? CHAN%d', chan)));
                vmax = str2double(get_query(obj, sprintf(':MEASure:VMAX? CHAN%d', chan)));
                send_cmd(obj, sprintf(':CHAN%d:RANG %f',chan, (vmax-vmin)*2));
                send_cmd(obj, sprintf(':CHAN%d:OFFS %f',chan, (vmin+vmax)/2));
                vmin = str2double(get_query(obj, sprintf(':MEASure:VMIN? CHAN%d', chan)));
                vmax = str2double(get_query(obj, sprintf(':MEASure:VMAX? CHAN%d', chan)));
                send_cmd(obj, sprintf(':CHAN%d:RANG %f',chan, (vmax-vmin)*1.3));
                send_cmd(obj, sprintf(':CHAN%d:OFFS %f',chan, (vmin+vmax)/2));
            end
        end
        function Data = get_data(obj)
            max_points = str2double(get_query(obj, ':ACQuire:POINts?'));
            send_cmd(obj, sprintf(':WAV:POINTS %d', max_points));
            % Now tell the instrument to digitize the channels
            send_cmd(obj, ':DIGITIZE CHAN1, CHAN2, CHAN3, CHAN4');

            % Wait till complete
            operationComplete = str2double(get_query(obj, '*OPC?'));
            while ~operationComplete
                operationComplete = str2double(get_query(obj, '*OPC?'));
            end
            % Get the data back as a WORD (i.e., INT16), other options are ASCII and BYTE
            send_cmd(obj, ':WAVEFORM:FORMAT WORD');
            % Set the byte order on the instrument as well
            send_cmd(obj, ':WAVEFORM:BYTEORDER LSBFirst');
            for chan = 1:4
                send_cmd(obj, sprintf(':WAVeform:SOURCE CHAN%d', chan));    
                % Get the preamble block
                preambleBlock = get_query(obj, ':WAVEFORM:PREAMBLE?');
                % The preamble block contains all of the current WAVEFORM settings.  
                % It is returned in the form <preamble_block><NL> where <preamble_block> is:
                %    FORMAT        : int16 - 0 = BYTE, 1 = WORD, 2 = ASCII.
                %    TYPE          : int16 - 0 = NORMAL, 1 = PEAK DETECT, 2 = AVERAGE
                %    POINTS        : int32 - number of data points transferred.
                %    COUNT         : int32 - 1 and is always 1.
                %    XINCREMENT    : float64 - time difference between data points.
                %    XORIGIN       : float64 - always the first data point in memory.
                %    XREFERENCE    : int32 - specifies the data point associated with
                %                            x-origin.
                %    YINCREMENT    : float32 - voltage diff between data points.
                %    YORIGIN       : float32 - value is the voltage at center screen.
                %    YREFERENCE    : int32 - specifies the data point where y-origin
                %                            occurs.
                % Now send commmand to read data
                send_cmd(obj, ':WAV:DATA?');
                % read back the BINBLOCK with the data in specified format and store it in
                % the waveform structure. FREAD removes the extra terminator in the buffer
                Data(chan).RawData = binblockread(obj.kohandle,'uint16'); fread(obj.kohandle,1);
                % Maximum value storable in a INT16
                maxVal = 2^16; 

                %  split the preambleBlock into individual pieces of info
                preambleBlock = regexp(preambleBlock,',','split');

                % store all this information into a waveform structure for later use
                Data(chan).Format = str2double(preambleBlock{1});     % This should be 1, since we're specifying INT16 output
                Data(chan).Type = str2double(preambleBlock{2});
                Data(chan).Points = str2double(preambleBlock{3});
                Data(chan).Count = str2double(preambleBlock{4});      % This is always 1
                Data(chan).XIncrement = str2double(preambleBlock{5}); % in seconds
                Data(chan).XOrigin = str2double(preambleBlock{6});    % in seconds
                Data(chan).XReference = str2double(preambleBlock{7});
                Data(chan).YIncrement = str2double(preambleBlock{8}); % V
                Data(chan).YOrigin = str2double(preambleBlock{9});
                Data(chan).YReference = str2double(preambleBlock{10});
                Data(chan).VoltsPerDiv = (maxVal * Data(chan).YIncrement / 8);      % V
                Data(chan).Offset = ((maxVal/2 - Data(chan).YReference) * Data(chan).YIncrement + Data(chan).YOrigin);         % V
                Data(chan).SecPerDiv = Data(chan).Points * Data(chan).XIncrement/10 ; % seconds
                Data(chan).Delay = ((Data(chan).Points/2 - Data(chan).XReference) * Data(chan).XIncrement + Data(chan).XOrigin); % seconds

                % Generate X & Y Data
                Data(chan).X = (Data(chan).XIncrement.*(1:length(Data(chan).RawData))) - Data(chan).XIncrement;
                Data(chan).Y = (Data(chan).YIncrement.*(Data(chan).RawData - Data(chan).YReference)) + Data(chan).YOrigin; 
            end
        end
    end
end

