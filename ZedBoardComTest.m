


ZB = ZBconnect('COM4');         % Verbinden
%%
ZB.BytesAvailable
ZBsetMux(ZB,1,2,1,2);            % Mux einstellen
ZBsetAmp(ZB,9);                  % Verst�rkung einstellen
%ZBsetOS(ZB,0);                   % Samplingrate beeinflussen
ZBnewmessurement(ZB);              % Neue Messung anfordern
CHA_roh = ZBgetCHraw(ZB,'A');    % Rohdaten CH A
CHB_roh = ZBgetCHraw(ZB,'B');    % Rohdaten CH B
% Rohdaten plotten
[ realP, imag ] = ZBgetFFT(ZB,'A');
CHA_FFT = complex(realP, imag)
i = 0:2047;
f=3;
x = sin(2*pi*i*f/2048);
%% AKF ausleden
try
        fprintf(ZB,'u');
catch MException
end;
%wait for bytes being available
while(ZB.BytesAvailable == 0)
end;
CHA_akf = fread(ZB, 2048, 'float');
%%
plot(CHA_akf(1:end-1))
%%
    X = fft(x, 2^nextpow2(length(x)-1));                                                % Mindestens doppelt so gro�e Datenbereich im FFT Raum anlegen. Warum?
    Cr = abs(X).^2;                                                                        % Betrag der komplexen, transformierten Amplitute quadrieren.
    c = real(ifft([0 Cr(1:end-1)]));                                                                 % Realteil der inversen FFT ermitteln.
    
    acor_ifftfft = shiftdim(c,1);                                                          % Dimension anpassen. 
    %acor_ifftfft = acor_ifftfft(1:length(x));      
plot(acor_ifftfft)
    hold off
%% 
Y= fft(x)
semilogx(abs(CHA_FFT))
hold on
semilogx(abs(Y)/2048)
grid on
hold off
%%
c = real(ifft([Cr(0:end)]));
plot(c*2024)


%% Flattop Fenster auf CH A legen lassen
ZBsetwindow(ZB);
%% Berechnungen anfordern
ZBdoCalc(ZB);

%% FFT CH A anfordern
[ real, imag ] = ZBgetFFT(ZB,'A');
%% Verarbeitung der Berechnungen
figure(2);
absoult = sqrt(real.^2+imag.^2);
fpgafft = [ absoult(2048); absoult(1:2047)];
matlabfft = abs(fft(CHA_roh,2048))/2048;
matlabfft_fenster = abs(fft(CHA_roh .*flattopwin(2048)*2048/sum(flattopwin(2048)),2048))/2048;
f  = fs*(0:(2047))/2048;
subplot(4,1,[1 2 3]);

plot(f, matlabfft_fenster);
hold on;
plot(f, fpgafft);
grid on;
legend('Matlab mit Fenster', 'FPGA')
hold off;
subplot(4,1,4);
semilogy(f, abs(matlabfft_fenster-fpgafft)/matlabfft_fenster);
grid on;
hold off;
%%
ZBdisconnect(ZB);