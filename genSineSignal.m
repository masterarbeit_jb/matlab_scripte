function [ y, t ] = genSineSignal(Amp, phi, dc, noise,periods, data_size);
%GENSIGNAL Summary of this function goes here
%   Detailed explanation goes here
t = (0:data_size-1);
y = Amp*(sin(2*pi*t*periods/data_size)+phi)+Amp*noise*randn(size(t))+dc;
end

