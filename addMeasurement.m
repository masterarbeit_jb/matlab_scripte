function [mes] = addMeasurement(typ, window, calc_rms, real_rms, periods, varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

pos = 1;
mes(pos).typ = typ;
mes(pos).window = window;
mes(pos).rms = calc_rms;
mes(pos).real_rms = real_rms;
mes(pos).rel_error = ((calc_rms-real_rms)./real_rms);
mes(pos).periods = periods;
if length(varargin) >=1;
    mes(pos).frequency = cell2mat (varargin(1));
else
    mes(pos).frequency = 0;
end


end

