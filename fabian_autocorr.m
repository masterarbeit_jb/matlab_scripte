function autocor = fabian_autocorr(signal)
subtractValue = mean(signal);

for tau = 1:(length(signal)/2)
    temp=0;
    for t = 0:(length(signal)-tau)
        temp = temp + ((signal(t+1)-subtractValue) * (signal(t+tau)-subtractValue));
    end;
    %%temp = temp / ((length(signal))-tau-1);
    autocor(tau) = temp;
end;
