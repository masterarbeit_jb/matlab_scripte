function [ f, Y ] = smartfft( y, Fs )
%SMARTFFT Summary of this function goes here
%   Detailed explanation goes here

L = length(y);

Y = fft(y);

P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
Y = P1;
f = Fs*(0:(L/2))/L;
end

