function [ rms_sb, f_main ] = sb_rms(x, f_sample)
%SB_RMS Summary of this function goes here
%   Detailed explanation goes here
    X = abs(fft(x, 2^nextpow2(length(x))));                                 % FFT des Signals durchf�hren % 2^nextpow2(length(x))
    X = 2*X/length(X);                                  % Amplitude verdoppelen (weil die Spiegelung weggeschnitten wird)
    X(1,1) = X(1,1)/2;                                  % f = 0 nicht von Spieglungsentfernung betroffen, daher r�ckg�ngig machen
    
    f = f_sample*(0:(length(X)-1))/length(X);                   % Frequenzverlauf skalieren
    f_main = f(peakfinder(X));                         % Hauptfrequenz finden
    rms_sb = X(peakfinder(X))/sqrt(2);               % Zugeh�rigen Effektivwert ermitteln
end